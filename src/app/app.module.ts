import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './../../../demo-ventas/src/app/material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductoComponent } from './_pages/producto/producto.component';
import { PersonaComponent } from './_pages/persona/persona.component';
import { ProductoModalComponent } from './_pages/producto/producto-modal/producto-modal.component';
import { PersonaEdicionComponent } from './_pages/persona/persona-edicion/persona-edicion.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    PersonaComponent,
    ProductoModalComponent,
    PersonaEdicionComponent
  ],
  entryComponents: [ProductoModalComponent], //aqui tenemos que indicar cuales seran los componentes modales
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule, //aqui importamos el modulo de material
    HttpClientModule, //modulo que nos permite hacer peticiones http
    ReactiveFormsModule, //modulo que nos permite la utilizacion de forms en angular
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
