import { HOST } from './../shared/var.constants';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Persona } from '../_model/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  url : string = HOST + '/personas';

  registroCambio = new Subject<Persona[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http : HttpClient) { }

  listar(){
    return this.http.get<Persona[]>(this.url);
  }

  listarPaginado(p : number, s : number){
    return this.http.get<Persona[]>(this.url + '/paginado?page=' + p + '&size=' + s);
  }

  listarPorId(id : number){
    return this.http.get<Persona>(this.url + '/' + id);
  }

  registrar(obj : Persona){
    return this.http.post<Persona>(this.url,obj);
  }

  modificar(obj : Persona){
    return this.http.put<Persona>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}
