import { Subject } from 'rxjs';
import { HOST } from './../../../../demo-ventas/src/app/shared/var.constants';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto } from '../_model/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  url : string = HOST + '/productos';

  registroCambio = new Subject<Producto[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http : HttpClient) { }

  listar(){
    return this.http.get<Producto[]>(this.url);
  }

  listarPaginado(p : number, s : number){
    return this.http.get<Producto[]>(this.url + '/paginado?page=' + p + '&size=' + s);
  }

  listarPorId(id : number){
    return this.http.get<Producto[]>(this.url + '/' + id);
  }

  registrar(obj : Producto){
    return this.http.post<Producto>(this.url,obj);
  }

  modificar(obj : Producto){
    return this.http.put<Producto>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}
