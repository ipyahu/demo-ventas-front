export const HOST = 'http://localhost:8085/demo.ventas';



export const MSJ_INS = 'El registro se ha insertado satisfactoriamente!';
export const MSJ_INS_ERR = 'Error en la insercion del registro!';
export const MSJ_MOD = 'El registro se ha modificado satisfactoriamente!';
export const MSJ_MOD_ERR = 'Error en la modificacion del registro!'
export const MSJ_DEL = 'El registro se ha eliminado satisfactoriamente!';
export const MSJ_DEL_ERR = 'Error en la eliminacion del registro!';