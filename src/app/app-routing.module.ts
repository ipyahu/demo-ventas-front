import { PersonaEdicionComponent } from './_pages/persona/persona-edicion/persona-edicion.component';
import { PersonaComponent } from './_pages/persona/persona.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductoComponent } from './_pages/producto/producto.component';

const routes: Routes = [
  {
    path: 'personas', component: PersonaComponent, children: [
      { path: 'nuevo', component: PersonaEdicionComponent },
      { path: 'edicion/:id', component: PersonaEdicionComponent }
    ]
  },
  { path: 'productos', component: ProductoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
