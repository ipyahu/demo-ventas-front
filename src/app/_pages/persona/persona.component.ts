import { ActivatedRoute } from '@angular/router';
import { MSJ_DEL, MSJ_DEL_ERR } from './../../shared/var.constants';
import { PersonaService } from './../../_service/persona.service';
import { MatTableDataSource, MatSnackBar, MatSort, MatPaginator } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Persona } from '../../_model/persona';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  listaDataSource: MatTableDataSource<Persona>; 
  columnasMostradas = ['idPersona','nombres','apellidos','acciones'];
  @ViewChild(MatPaginator) paginator : MatPaginator; 
  @ViewChild(MatSort) sort :MatSort; 
  cantRegistros : number;

  constructor(private service : PersonaService, private alerta :  MatSnackBar, public route : ActivatedRoute) { }

  ngOnInit() {
    this.service.registroCambio.subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;      
    });

    this.service.mensajeCambio.subscribe(mensaje => {
      this.alerta.open(mensaje,'Aviso',{duration:3000});
    });

    this.service.listarPaginado(0,10).subscribe(data => {
      let registros = JSON.parse(JSON.stringify(data)).content;
      this.cantRegistros = JSON.parse(JSON.stringify(data)).totalElements;
      this.listaDataSource = new MatTableDataSource(registros);
      this.listaDataSource.sort = this.sort;      
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.listaDataSource.filter = filterValue;
  }

  eliminar(id : number){
    this.service.eliminar(id).subscribe(data => {
      this.service.listar().subscribe(data => {
        this.service.registroCambio.next(data);
        this.service.mensajeCambio.next(MSJ_DEL);
      });
    },error => {
      this.service.mensajeCambio.next(MSJ_DEL_ERR);
    });
  }

  mostrarMas(e : any){
    this.service.listarPaginado(e.pageIndex, e.pageSize).subscribe(data => {
       let registros = JSON.parse(JSON.stringify(data)).content;
       this.cantRegistros = JSON.parse(JSON.stringify(data)).totalElements;
       this.listaDataSource = new MatTableDataSource(registros);  
       this.listaDataSource.sort = this.sort;     
    });
  }

}
