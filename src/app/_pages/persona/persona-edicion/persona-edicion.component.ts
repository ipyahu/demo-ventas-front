import { MSJ_MOD, MSJ_INS } from './../../../../../../mediapp/src/app/shared/var.constants';
import { PersonaService } from './../../../_service/persona.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { MSJ_MOD_ERR, MSJ_INS_ERR } from './../../../shared/var.constants';
import { Component, OnInit } from '@angular/core';
import { Persona } from '../../../_model/persona';

@Component({
  selector: 'app-persona-edicion',
  templateUrl: './persona-edicion.component.html',
  styleUrls: ['./persona-edicion.component.css']
})
export class PersonaEdicionComponent implements OnInit {


  id : number;
  form : FormGroup; 
  edicion : boolean = false; 
  registro : Persona;

  constructor(private route : ActivatedRoute,private router : Router, private service : PersonaService) {
    this.form = new FormGroup({
      'id': new FormControl(0), 
      'nombres' : new FormControl(''),
      'apellidos': new FormControl('')
    });
  } 

  ngOnInit() {
    this.registro = new Persona();
    this.route.params.subscribe( (parametros : Params) => { 
      this.id = parametros['id'];
      this.edicion = parametros['id'] != null; 
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      this.service.listarPorId(this.id).subscribe(datos => { 
        this.form = new FormGroup({
          'id': new FormControl(datos.idPersona),
          'nombres' : new FormControl(datos.nombres),
          'apellidos': new FormControl(datos.apellidos)
        });
      });
    }
  }


  operar(){
    this.registro.idPersona = this.form.value['id'];
    this.registro.nombres = this.form.value['nombres'];
    this.registro.apellidos = this.form.value['apellidos'];

    if (this.id){
      this.service.modificar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_MOD);
        });
      }, error => {
        this.service.mensajeCambio.next(MSJ_MOD_ERR);
      });
    }
    else{
      this.service.registrar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_INS);
        });
      }, error => {
        this.service.mensajeCambio.next(MSJ_INS_ERR);
      });      
    }

    this.router.navigate(['personas']);
  }

}
