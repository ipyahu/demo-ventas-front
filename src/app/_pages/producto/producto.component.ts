import { MSJ_DEL_ERR } from './../../shared/var.constants';
import { ProductoModalComponent } from './producto-modal/producto-modal.component';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { Producto } from './../../_model/producto';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductoService } from '../../_service/producto.service';
import { MSJ_DEL } from '../../shared/var.constants';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  obj : Producto;

  listaDataSource: MatTableDataSource<Producto>; 
  columnasMostradas = ['idProducto','nombre','marca','acciones'];
  @ViewChild(MatPaginator) paginator : MatPaginator; 
  @ViewChild(MatSort) sort :MatSort; 
  cantRegistros : number;

  constructor(private service : ProductoService, private alerta :  MatSnackBar, private modal : MatDialog) { }

  ngOnInit() {
    this.service.registroCambio.subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;      
    });

    this.service.mensajeCambio.subscribe(mensaje => {
      this.alerta.open(mensaje,'Aviso',{duration:3000});
    });

    this.service.listarPaginado(0,10).subscribe(datos => {
      let registros = JSON.parse(JSON.stringify(datos)).content;
      this.cantRegistros = JSON.parse(JSON.stringify(datos)).totalElements;
      this.listaDataSource = new MatTableDataSource(registros);
      this.listaDataSource.sort = this.sort; 
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.listaDataSource.filter = filterValue;
  }

  mostrarMas(e : any){
    this.service.listarPaginado(e.pageIndex, e.pageSize).subscribe(datos => {
      let registros = JSON.parse(JSON.stringify(datos)).content;
      this.listaDataSource = new MatTableDataSource(registros);
      this.listaDataSource.sort = this.sort;      
    });
  }

  eliminar(id : number){
    this.service.eliminar(id).subscribe(data => {
      this.service.listar().subscribe(data => {
        this.service.registroCambio.next(data);
        this.service.mensajeCambio.next(MSJ_DEL);
      })
    }, error => {
      this.service.mensajeCambio.next(MSJ_DEL_ERR);
    })
  }

  abrirModal(obj : Producto){
    let registro = obj != null ? obj : new Producto();
    this.modal.open(ProductoModalComponent, {
      width: '400px',
      disableClose : true,
      data : registro
    })
  }

}
