import { MSJ_INS } from './../../../shared/var.constants';
import { ProductoService } from './../../../_service/producto.service';
import { Producto } from './../../../_model/producto';
import { Component, OnInit, inject, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MSJ_MOD } from '../../../shared/var.constants';

@Component({
  selector: 'app-producto-modal',
  templateUrl: './producto-modal.component.html',
  styleUrls: ['./producto-modal.component.css']
})
export class ProductoModalComponent implements OnInit {

  registro : Producto;

  constructor(private modalRef : MatDialogRef<ProductoModalComponent>,
              private service : ProductoService,
              @Inject(MAT_DIALOG_DATA) public obj : Producto) 
  {

  }

  ngOnInit() {
    this.registro = new Producto();
    this.registro.idProducto = this.obj.idProducto;
    this.registro.nombre = this.obj.nombre;
    this.registro.marca = this.obj.marca;
  }

  cancelar(){
    this.modalRef.close();
  }

  operar(){
    if (this.registro != null && this.registro.idProducto > 0){
      this.service.modificar(this.registro).subscribe(data => {
        this.service.listar().subscribe(data => {
          this.service.registroCambio.next(data);
          this.service.mensajeCambio.next(MSJ_MOD);
        })
      });
    }else{
      this.service.registrar(this.registro).subscribe(data => {
        this.service.listar().subscribe(data => {
          this.service.registroCambio.next(data);
          this.service.mensajeCambio.next(MSJ_INS);
        })
      });      
    }
    this.modalRef.close();
  }

}
